Zcash Improvement Proposals (ZIPs)
==================================

Abstract
--------

A Zcash Improvement Proposal (ZIP) is a design document providing information to the Zcash community, or describing a new feature for Zcash or its processes or environment. The ZIP should provide a concise technical specification of the feature and a rationale for the feature.

We intend ZIPs to be the primary mechanism for proposing new features, for collecting community input on an issue, and for documenting the design decisions that have gone into Zcash. The author(s) of the ZIP are responsible for building consensus within the community and documenting dissenting opinions.

Because the ZIPs are maintained as text files in a versioned repository, their revision history is the historical record of the feature proposal.

See `ZIP 0 <https://github.com/zcash/zips/blob/master/zip-0000.rst>`_ for complete details including workflow, formatting and management.

See the `ZIP repository in GitHub <https://github.com/zcash/zips/>`_ for a full list of accepted proposals and the `pull requests for that repository <https://github.com/zcash/zips/pulls>`_ for drafts and suggested updates.


